Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: inetsim
Source: https://www.inetsim.org/index.html
Files-Excluded: *.exe
Comment: Source has been repacked to remove those files

Files: *
Copyright: 2007-2019 Thomas Hungenberg & Matthias Eckert
License: GPL-2+

Files: debian/*
Copyright: 2013 Devon Kearns <dookie@kali.org>
           2017 GengYu Rao <zouyoo@outlook.com>
           2017-2019 Thomas Hungenberg
           2019 Sophie Brun <sophie@offensive-security.com>
License: GPL-2+

Files: lib/INetSim/Fork.pm
Copyright: 2001-2007 Paul Seamons <paul@seamons.com>
           2007-2019 Thomas Hungenberg <th@inetsim.org>, Matthias Eckert <me@inetsim.org>
License: GPL-1+ or Artistic

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
 
License: Artistic
 This program is free software; you can redistribute it and/or modify it
 under the terms of the "Artistic License".
 .
 THIS PACKAGE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES
 OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 .
 On Debian systems, the complete text of the Artistic License
 can be found in "/usr/share/common-licenses/Artistic".
 
License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
