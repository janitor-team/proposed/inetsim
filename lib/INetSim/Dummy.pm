# -*- perl -*-
#
# INetSim::Dummy - Base package for Dummy::TCP and Dummy::UDP
#
# (c)2008-2019 Matthias Eckert, Thomas Hungenberg
#
#############################################################

package INetSim::Dummy;

use strict;
use warnings;
use base qw(INetSim::GenericServer);

# no shared functions

1;
#
